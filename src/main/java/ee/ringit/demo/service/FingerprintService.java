package ee.ringit.demo.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Service;

@Service
public class FingerprintService {
	static final Map<String, String> MAP = new HashMap<String, String>();
	static {
		MAP.put("a", "а");
		MAP.put("b", "ᖯ");
		MAP.put("c", "с");
		MAP.put("d", "ԁ");
		MAP.put("e", "е");
		MAP.put("f", "ꬵ");
		MAP.put("g", "ᶃ");
		MAP.put("h", "հ");
		MAP.put("i", "і");
		MAP.put("j", "ј");
		MAP.put("k", "κ");
		MAP.put("l", "ӏ");
		MAP.put("m", "rn");
		MAP.put("n", "ո");
		MAP.put("o", "ο");
		MAP.put("p", "р");
		MAP.put("q", "ԛ");
		MAP.put("r", "ꭈ");
		MAP.put("s", "ѕ");
		MAP.put("t", "ʈ");
		MAP.put("u", "ս");
		MAP.put("v", "ν");
		MAP.put("w", "ԝ");
		MAP.put("y", "у");
		MAP.put("z", "ᴢ");
	}

	public String fingerprint(InputStream inputStream, String watermark) throws IOException {
		InputStreamReader isr = new InputStreamReader(inputStream, "UTF-8");
        Reader in = new BufferedReader(isr);
        int inputCharacter;

		StringBuffer result = new StringBuffer();
		String watermarkInProgress = new String(watermark.toLowerCase());
		
        while ((inputCharacter = in.read()) > -1) {
			if (watermarkInProgress.length() > 0)
			{
				verifyThatNoExistingConfusables((char)inputCharacter);

				char watermarkCurrentChar = watermarkInProgress.charAt(0);
				if ((char)inputCharacter == watermarkCurrentChar)
				{
					result.append((String)MAP.get(String.valueOf((char)inputCharacter)));
					watermarkInProgress = watermarkInProgress.substring(1);
					continue;
				}
			}
			result.append((char)inputCharacter);
		}
		return result.toString();
	}

	public String extract(InputStream inputStream) throws IOException {
		InputStreamReader isr = new InputStreamReader(inputStream, "UTF-8");
        Reader in = new BufferedReader(isr);
        int inputCharacter;
        
		StringBuffer result = new StringBuffer();
        while ((inputCharacter = in.read()) > -1) {
			String character = String.valueOf((char)inputCharacter);
			if (MAP.values().contains(character)) {
				result.append((char)inputCharacter);
			}
		}
		return result.toString();
	}

	private void verifyThatNoExistingConfusables(char inputCharacter) {
		if (MAP.containsValue(String.valueOf(inputCharacter))) {
			throw new RuntimeException("Cannot process, confusabled already existing in text!");
		}
	}
}
