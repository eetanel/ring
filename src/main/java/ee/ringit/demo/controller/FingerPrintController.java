package ee.ringit.demo.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import ee.ringit.demo.service.FingerprintService;

@Controller
public class FingerPrintController {

	@Autowired
	FingerprintService fingerprintService;

	@PostMapping("/fingerprint")
	public ResponseEntity<String> fingerprint(@RequestParam("watermark") String watermark, 
			@RequestParam("file") MultipartFile uploadfile) throws IOException {
		return new ResponseEntity<String>(fingerprintService.fingerprint(uploadfile.getInputStream(), watermark), HttpStatus.OK);
	}
	
	@PostMapping("/identify")
	public ResponseEntity<String> identify(@RequestParam("file") MultipartFile uploadfile) throws IOException {
		return new ResponseEntity<String>(fingerprintService.extract(uploadfile.getInputStream()), HttpStatus.OK);
	}
}
