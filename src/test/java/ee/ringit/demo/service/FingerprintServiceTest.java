package ee.ringit.demo.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

public class FingerprintServiceTest {
	FingerprintService fingerPrintService = new FingerprintService();

	@Test
	public void testFingerprint() throws IOException {
		InputStream input = new ByteArrayInputStream("Pikk jänes toimetas kui hobune!".getBytes("UTF-8"));
		String watermark = "john";

		String result = this.fingerPrintService.fingerprint(input, watermark);

		Assert.assertThat(result, Matchers.is(Matchers.equalTo("Pikk јänes tοimetas kui հobuոe!")));
	}

	@Test
	public void testExtract() throws IOException {
		InputStream input = new ByteArrayInputStream("Pikk јänes tοimetas kui հobuոe!".getBytes("UTF-8"));

		String result = this.fingerPrintService.extract(input);

		Assert.assertThat(result, Matchers.is(Matchers.equalTo("јοհո")));
	}

	@Test(expected=RuntimeException.class)
	public void testUnSupportedCase() throws IOException {
		this.fingerPrintService.fingerprint(new ByteArrayInputStream("Pikk јänes tοimetas kui հobuոe!".getBytes("UTF-8")), "sein");
	}

	@Test
	public void testMapIntegrity() {
		Assert.assertThat(FingerprintService.MAP.keySet(), Matchers.not(Matchers.contains(new Object[] { FingerprintService.MAP.values() })));
	}
}
