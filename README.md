# Fingerprinting
Spring Boot application (Jetty) for fingerprinting text with watermark or extracting watermark from previusly watermarked text.

## For building:
mvn package

## For running:
java -jar fingerprint-0.0.1-SNAPSHOT.jar

## For testing:
###### Create fingerprinted version of the book for John:
* curl -F watermark=John -F file=@"src/test/resources/book.txt" http://localhost:8080/fingerprint -o book_john.txt

######Extract book's fingerprint:
* curl -F file=@book_john.txt http://localhost:8080/identify
